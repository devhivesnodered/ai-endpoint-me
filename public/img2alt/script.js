document.getElementById('imageForm').addEventListener('submit', async function(event) {
    event.preventDefault();
    
    const apiKeyInput = document.getElementById('apiKeyInput');
    const apiKey = apiKeyInput.value;
    if (!apiKey) {
        alert('Please enter your API key');
        return;
    }
    
    const imageInput = document.getElementById('imageInput');
    const file = imageInput.files[0];
    if (!file) {
        alert('Please select an image file');
        return;
    }

    const reader = new FileReader();
    reader.onload = async function() {
        const base64Image = reader.result.split(',')[1];
        const prompt = "Generate a detailed description of the following image: ";
        const response = await sendImageToHuggingFace(base64Image, prompt, apiKey);
        displayResponse(response, reader.result);  // Pass the base64 URL to display the image
    };
    reader.readAsDataURL(file);
});

async function sendImageToHuggingFace(imageBase64, prompt, apiKey) {
    const url = 'https://api-inference.huggingface.co/models/Salesforce/blip-image-captioning-base';
    const headers = {
        'Authorization': `Bearer ${apiKey}`,
        'Content-Type': 'application/json'
    };
    const payload = {
        "inputs": {
            "image": imageBase64,
            "prompt": prompt
        }
    };

    try {
        const response = await fetch(url, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(payload)
        });
        
        if (!response.ok) {
            const errorText = await response.text();
            throw new Error(`HTTP error! status: ${response.status}, response: ${errorText}`);
        }

        const data = await response.json();
        console.log('Response from API:', data);  // Debugging line
        return data[0].generated_text || 'No response field in API response';
    } catch (error) {
        console.error('Error:', error);
        return `Error processing the image: ${error.message}`;
    }
}

function displayResponse(response, imageUrl) {
    const responseText = document.getElementById('responseText');
    const uploadedImage = document.getElementById('uploadedImage');

    responseText.textContent = response;
    uploadedImage.src = imageUrl;
    uploadedImage.style.display = 'block';
}
